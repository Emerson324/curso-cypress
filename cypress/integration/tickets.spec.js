describe('Tickets', () => {
    beforeEach(() => {
        cy.visit("https://ticket-box.s3.eu-central-1.amazonaws.com/index.html")
    });

    it("fills all the text input fields",()=>{
        const firstName = "Emerson";
        const lastName = "Cardoso";

        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("emerson_ti_co@hotmail.com");
        cy.get("#requests").type("Vegetarian");
        cy.get("#signature").type(`${firstName} ${lastName}`);
    });

    it("select two tickets", () => {
        cy.get('#ticket-quantity').select("2");
    });

    it("select 'vip' ticket type", () => {
        cy.get('#vip').check();
    })

    it("selects 'social media' checkbox",() => {
        cy.get("#social-media").check();
    })

    it("selects 'friends', and 'puclication', them uncheck 'friend'", () => {
        cy.get("#friend").check();
        cy.get("#publication").check();
        cy.get("#friend").uncheck();
    })
    
    it("has 'TICKETBOX' header's heading", () => {
        cy.get("header h1").should("contain", "TICKETBOX");
    });

    it("alert on invalid email", () => {
        cy.get("#email")
          .as("email") //apelidou seletor css #email de email
          .type("emerson-hotmail.com");

        cy.get("#email.invalid")
          .as("invalidEmail")
          .should("exist");

        cy.get("@email") //chamou o seletor css #email, lembrar quando chamar o alias deverá estar na frente @
          .clear() //limpa o campo no qual está o email
          .type("emerson_ti_co@hotmail.com");

        //nesse caso não posso reaproveitar o alias, pois ele guarda o estado do elemento quando foi localizado
        //com isso como ele foi utilizado uma vez e já foi guardado o estado não poderei usar mais uma vez  
        cy.get("#email.invalid") 
          .as("invalidEmail")
          .should("not.exist");
    })

    it("fills and reset the form", () => {
        const firstName = "Emerson";
        const lastName = "Cardoso";
        const fullName = `${firstName} ${lastName}`

        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("emerson_ti_co@hotmail.com");
        cy.get('#ticket-quantity').select("2");
        cy.get('#vip').check();
        cy.get("#friend").check();
        cy.get("#requests").type("IPA beer");

        cy.get(".agreement p").should(
            "contain", 
            `I, ${fullName}, wish to buy 2 VIP tickets.`
        );

        cy.get("#agree").click();
        cy.get("#signature").type(fullName);
        
        cy.get("button[type='submit']")
          .as("submitButton")
          .should("not.be.disabled")

        cy.get("button[type='reset']").click();

        cy.get("@submitButton").should("be.disabled");
        
    })

    it("fills mandatory fields using support command", () => {
        const customer = {
            firstName: "João",
            lastName: "Silva",
            email: "joaosilva@hotmail.com"
        };

        cy.fillMandatoryFields(customer);

        cy.get("button[type='submit']")
          .as("submitButton")
          .should("not.be.disabled");

        cy.get("#agree").uncheck();

        cy.get("@submitButton").should("be.disabled");
    })
});